# Example Code of Acamica Node.js for Globant

Link to course: https://www.acamica.com/cursos/410/

The example application is a server to collect poll results. Polls can be created
via an REST API and can be voted upon via REST or Socket.io connections. Results are
persisted in a Redis backend.

## Structure

Apart from the complete application in the root directory, in the `./chapters`
subdirectories you'll find the example code corresponding to each lesson in Acamica.

- `./`: main application
  - `./db`: database connector
  - `./polls`: polls model and routes
  - `./votes`: votes model and routes
  - `./config`: app config
  - `./test`: test cases
- `./chapters`: code related to each chapter

## Usage

**Installation**
``` bash
npm install
```

**Important:** You need to execute `npm install` **in each** chapters folder.

In order to run the application a [Redis Server](https://redis.io/) needs to be
listening on `localhost:6739`.

**Run tests**
``` bash
npm test
```

**Run app**
``` bash
node index.js
```
Go to: http://localhost:8080/

## Sources
- https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/
- https://expressjs.com/en/guide/routing.html
