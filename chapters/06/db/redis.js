let redis;

if (!redis) {
  const Redis = require('ioredis');

  redis = new Redis();
}

module.exports = redis;
