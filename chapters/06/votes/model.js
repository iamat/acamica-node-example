const redis = require('../db/redis.js');

const Poll = require('../polls/model.js');
const async = require('async');

const VOTE_PREFIX = 'vote';

function recordVote (id, choice, timestamp, user, callback) {
  redis.set(
    `${VOTE_PREFIX}:${id}:${timestamp}`,
    JSON.stringify({ id, choice, timestamp, user }),
    callback
  );
}

function incCounter (id, choice, callback) {
  redis.hincrby(
    `${VOTE_PREFIX}:count:${id}`,
    choice,
    1,
    callback
  );
}

function vote (id, choice, user, callback) {
  Poll.get(id, (err, poll) => {
    if (err) return callback(err);

    if (!poll) return callback(new Error('POLL_NOT_FOUND'));

    async.parallel({
      recordVote: (done) => recordVote(id, choice, Date.now(), user, done),
      incCounter: (done) => incCounter(id, choice, done)
    }, (err, results) => {
      if (err) return callback(err);

      callback();
    });
  });
}

function get (id, callback) {
  async.parallel({
    poll: (done) => Poll.get(id, done),
    count: (done) => redis.hgetall(`${VOTE_PREFIX}:count:${id}`, done)
  }, (err, results) => {
    if (err) return callback(err);
    if (!results.poll) return callback(new Error('POLL_NOT_FOUND'));

    console.log(results);

    results.poll.results = results.poll.options.map((option, i) => {
      return {
        text: option,
        count: Number(results.count[i]) || 0
      };
    });

    callback(null, results.poll);
  });
}

module.exports = {
  get,
  vote
};
