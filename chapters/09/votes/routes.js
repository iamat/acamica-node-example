const Votes = require('./model');

function getStream (req, res, next) {
  const id = req.params.id;

  const voteStream = Votes.createStream(id);

  voteStream.on('error', (err) => {
    console.error(`Something went wrong while streaming votes: ${err.message}`);
  });

  res.statusCode = 200;
  res.setHeader('Content-type', 'application/x-json-stream');

  voteStream.pipe(res);
}

function get (req, res, next) {
  const id = req.params.id;

  Votes.get(id, (err, results) => {
    if (err) return next(err);

    res.send(results);
  });
}

function vote (req, res, next) {
  const id = req.params.id;
  const choice = req.body.choice;
  const user = req.body.user;

  Votes.vote(id, choice, user, (err) => {
    if (err && err.message === 'POLL_NOT_FOUND') {
      console.warn({ poll: { id }, choice, user }, 'vote, but poll not found');
      res.status(404).send({ error: err.message });
      return;
    }

    if (err) return next(err);

    console.log({ poll: { id }, choice, user }, 'vote received');
    res.sendStatus(204);
  });
}

module.exports = {
  getStream,
  get,
  vote
};
