const path = require('path');
const app = require('express')();
const server = require('http').Server(app);

const io = require('socket.io')(server);

const bodyParser = require('body-parser');

const pollRoutes = require('./polls/routes');

const voteModel = require('./votes/model');
const voteRoutes = require('./votes/routes');

server.listen(8080); // Activates this server, listening on port 8080.
console.log('Server listenting on port 8080');

app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/polls', pollRoutes.getAll);
app.post('/polls', pollRoutes.create);
app.get('/polls/:id', pollRoutes.get);
app.put('/polls/:id', pollRoutes.update);
app.delete('/poll/:id', pollRoutes.remove);

app.get('/votes/:id', voteRoutes.get);
app.put('/votes/:id', voteRoutes.vote);

io.on('connection', (socket) => {
  socket.on('vote', (data) => {
    voteModel.vote(data.id, data.choice, data.user, () => {
      voteModel.get(data.id, (err, poll) => {
        if (err) {
          console.error(err);
          return;
        }
        socket.emit('new_result', poll);
      });
    });
  });
});
