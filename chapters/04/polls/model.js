const pollsStore = {};

const POLL_PREFIX = 'poll';

function get (id, callback) {
  const poll = pollsStore[`${POLL_PREFIX}:${id}`];

  callback(null, poll);
}

function create (poll, callback) {
  const id = Math.floor((Math.random() * 1e6)).toString();

  pollsStore[`${POLL_PREFIX}:${id}`] = poll;

  callback(null, { id, poll });
}

module.exports = {
  create,
  get
};
