const async = require('async');

const polls = require('./polls/model');
const votes = require('./votes/model');

const poll = {
  question: 'Do you like cake?',

  options: ['yes', 'no']
};

async.waterfall([(done) => {
  polls.create(poll, (err, result) => {
    done(err, result);
  });
}, (createResult, done) => {
  votes.vote(createResult.id, 0, 'Juan Carlos', (err) => {
    done(err, createResult);
  });
}, (createResult, done) => {
  votes.vote(createResult.id, 1, 'Lars', (err) => {
    done(err, createResult);
  });
}, (createResult, done) => {
  votes.get(createResult.id, done);
}], (err, result) => {
  if (err) throw err;

  console.log(result);
});
