const polls = require('../polls/model.js');
const async = require('async');

const VOTE_PREFIX = 'vote';

const voteStore = {};
const countStore = {};

function recordVote (id, choice, timestamp, user, callback) {
  voteStore[`${VOTE_PREFIX}:${id}:${timestamp}`] = { id, choice, timestamp, user };

  callback();
}

function incCounter (id, choice, callback) {
  const key = `${VOTE_PREFIX}:count:${id}`;
  if (!countStore[key]) {
    countStore[key] = {};
  }

  countStore[key][choice] =
    (countStore[key][choice] || 0) + 1;

  callback();
}

function vote (id, choice, user, callback) {
  polls.get(id, (err, poll) => {
    if (err) return callback(err);

    if (!poll) return callback(new Error('POLL_NOT_FOUND'));

    async.parallel({
      recordVote: (done) => recordVote(id, choice, Date.now(), user, done),
      incCounter: (done) => incCounter(id, choice, done)
    }, (err, results) => {
      if (err) return callback(err);

      callback();
    });
  });
}

function get (id, callback) {
  polls.get(id, (err, poll) => {
    if (err) return callback(err);

    if (!poll) return callback(new Error('POLL_NOT_FOUND'));

    const count = countStore[`${VOTE_PREFIX}:count:${id}`] || {};

    poll.results = poll.options.map((option, i) => {
      return {
        text: option,
        count: count[i] || 0
      };
    });

    callback(null, poll);
  });
}

module.exports = {
  get,
  vote
};
