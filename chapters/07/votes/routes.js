const Votes = require('./model');

function get (req, res, next) {
  const id = req.params.id;

  Votes.get(id, (err, results) => {
    if (err) return next(err);

    res.send(results);
  });
}

function vote (req, res, next) {
  const id = req.params.id;
  const choice = req.body.choice;
  const user = req.body.user;

  Votes.vote(id, choice, user, (err) => {
    if (err && err.message === 'POLL_NOT_FOUND') {
      console.warn({ poll: { id }, choice, user }, 'vote, but poll not found');
      res.status(404).send({ error: err.message });
      return;
    }

    if (err) return next(err);

    console.log({ poll: { id }, choice, user }, 'vote received');
    res.sendStatus(204);
  });
}

module.exports = {
  get,
  vote
};
