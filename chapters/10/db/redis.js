let redis;

if (!redis) {
  const cfgManager = require('node-config-manager');
  const cfgRedis = cfgManager
        .addConfig('redis')
        .getConfig('redis');

  const Redis = require('ioredis');

  redis = new Redis({
    hostname: cfgRedis.hostname,
    port: cfgRedis.port,
    db: cfgRedis.db
  });
}

module.exports = redis;
