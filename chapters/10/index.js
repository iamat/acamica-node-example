const cfgManager = require('node-config-manager');
const path = require('path');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: 'polls' });

const express = require('express');
const app = express();

const server = require('http').Server(app);
const bodyParser = require('body-parser');
const io = require('socket.io')(server);

const cfgServer = cfgManager
      .addConfig('server')
      .getConfig('server');

const pollRoutes = require('./polls/routes');
const voteModel = require('./votes/model');

const voteRoutes = require('./votes/routes');

server.listen(cfgServer.port); // Activates this server
log.info({ port: cfgServer.port }, 'Server started');

app.use(bodyParser.json());
app.use((req, res, next) => {
  // attach standard logger
  req.log = bunyan.createLogger({
    name: 'pollsRoute',
    serializers: bunyan.stdSerializers,
    req
  });

  return next();
});

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/polls', pollRoutes.getAll);
app.post('/polls', pollRoutes.create);
app.get('/polls/:id', pollRoutes.get);
app.put('/polls/:id', pollRoutes.update);
app.delete('/poll/:id', pollRoutes.remove);

app.get('/votes/:id', voteRoutes.get);
app.get('/votes/:id/stream', voteRoutes.getStream);
app.put('/votes/:id', voteRoutes.vote);

io.on('connection', (socket) => {
  socket.on('vote', (data) => {
    voteModel.vote(data.id, data.choice, data.user, () => {
      voteModel.get(data.id, (err, poll) => {
        if (err) {
          console.error(err);
          return;
        }
        socket.emit('new_result', poll);
      });
    });
  });
});
