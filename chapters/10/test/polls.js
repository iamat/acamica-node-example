/* eslint-env node, mocha */

const redis = require('../db/redis.js');

const assert = require('assert');

const polls = require('../polls/model.js');

// switch to DB 5 and
// clean up DB before test run
// so we won't overwrite our local data
before(function (done) {
  redis.select(5);
  redis.flushdb(done);
});

// clean up DB after test run
after(function (done) {
  redis.flushdb(done);
});

// passing arrow functions is discouraged
// mocha uses the binding to this for transmitting context
describe('polls', function () {
  describe('#create()', function () {
    it('should return an id and the created poll', function (done) {
      const poll = {
        question: 'Do you like cake?',
        options: ['yes', 'no']
      };

      polls.create(poll, (err, result) => {
        if (err) { throw err; }

        assert.ok(result.id, 'has key id');
        assert.deepEqual(result.poll, poll, 'result poll is the same');
        done();
      });
    });
  });
});
