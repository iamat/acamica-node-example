/* eslint-env node, mocha */

const redis = require('../db/redis.js');

const assert = require('assert');

const votes = require('../votes/model.js');

// switch to DB 5 and
// clean up DB before test run
// so we won't overwrite our local data
before(function (done) {
  redis.select(5);
  redis.flushdb(done);
});

// clean up DB after test run
after(function (done) {
  redis.flushdb(done);
});

describe('votes', function () {
  describe('#vote()', function () {
    it('should return err when poll not found', function (done) {
      votes.vote('someId', 2, 'me', (err, result) => {
        assert.ok(err, 'return error object');
        assert.equal(err.message, 'POLL_NOT_FOUND', 'with message "POLL_NOT_FOUND"');
        done();
      });
    });
  });
});
