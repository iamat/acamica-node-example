/**
 * Pure http example
 */
const http = require('http');

http.createServer((request, response) => {
  // Extracting headers, method and url from the request object
  const { headers, method, url } = request;
  let body = [];

  // The request object is a readable stream.
  // Don't worry about this too much, we will look into this later
  request.on('error', (err) => { // Handling read errors
    console.error(err);
  }).on('data', (chunk) => { // Collect sent data
    body.push(chunk);
  }).on('end', () => {
    // When the stream ends we put all of it together and convert
    // it into a string
    body = Buffer.concat(body).toString();

    // The response object is a writable stream
    response.on('error', (err) => { // Handling write errors
      console.error(err);
    });

    // Compiling data to send
    // This server will respond echoing the input
    const message = '¡Hola Globant!';
    const responseBody = { message, headers, method, url, body };

    response.statusCode = 200; // Setting the HTTP status code
    response.setHeader('Content-Type', 'application/json');

    // Writng the data to the stream.
    // The written data can be only a string or a buffer
    response.write(JSON.stringify(responseBody));

    // When all data is written to the stream the .end() method is called
    // and the response will be closed
    response.end();
  });
}).listen(8080); // Activates this server, listening on port 8080.
