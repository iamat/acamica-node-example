/**
 * Express example
 */
const express = require('express');
const app = express();

// respond with "¡Hola Globant!" when a GET request is made to the homepage
app.use('*', function (request, response) {
  const { headers, method, originalUrl, body } = request;

  // Compiling data to send
  // This server will respond echoing the input
  const message = '¡Hola Globant!';
  const responseBody = { message, headers, method, url: originalUrl, body };

  // The response can be send out with a single method .send()
  // Content-Type and statusCode will be set implicitly depending on the
  // data type passed to the method.
  response.send(responseBody);
});

app.listen(8080); // Activates this server, listening on port 8080.
