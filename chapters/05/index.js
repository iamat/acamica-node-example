const express = require('express');
const app = express();

const bodyParser = require('body-parser');

const pollRoutes = require('./polls/routes');

const voteRoutes = require('./votes/routes');

app.listen(8080); // Activates this server, listening on port 8080.
console.log('Server listenting on port 8080');

app.use(bodyParser.json());

app.get('/polls', pollRoutes.getAll);
app.post('/polls', pollRoutes.create);
app.get('/polls/:id', pollRoutes.get);
app.put('/polls/:id', pollRoutes.update);
app.delete('/poll/:id', pollRoutes.remove);

app.get('/votes/:id', voteRoutes.get);
app.put('/votes/:id', voteRoutes.vote);
