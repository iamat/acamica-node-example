const pollsStore = {};
const POLL_PREFIX = 'poll';

function getAll (callback) {
  const polls = Object.keys(pollsStore).map((key) => {
    return pollsStore[key];
  });

  callback(null, polls);
}

function get (id, callback) {
  const poll = pollsStore[`${POLL_PREFIX}:${id}`];

  callback(null, poll);
}

function create (poll, callback) {
  const id = Math.floor((Math.random() * 1e6)).toString();

  poll.id = id;
  pollsStore[`${POLL_PREFIX}:${id}`] = poll;

  callback(null, { id, poll });
}

function update (id, poll, callback) {
  poll.id = id;
  pollsStore[`${POLL_PREFIX}:${id}`] = poll;

  callback(null, { id, poll });
}

function remove (id, callback) {
  delete pollsStore[`${POLL_PREFIX}:${id}`];
  callback();
}

module.exports = {
  getAll,
  create,
  get,
  update,
  remove
};
