# Create Poll
curl -d '{"question":"Do you like cake?","options":["yes","no"]}' \
     -H "Content-Type: application/json" \
     -X POST http://localhost:8080/polls
# result: {"poll":{"id":"420010","poll":{"question":"Do you like cake?","options":["yes","no"],"id":"420010"}}}

# First vote
curl -d '{"choice":0,"user":"Juan Carlos"}' \
     -H "Content-Type: application/json" \
     -X PUT http://localhost:8080/votes/420010

# Second vote
curl -d '{"choice":1,"user":"Lars"}' \
     -H "Content-Type: application/json" \
     -X PUT http://localhost:8080/votes/420010

# Get results
curl http://localhost:8080/votes/420010 | jq .
# Result:
# {
#     "question": "Do you like cake?",
#     "options": [
#         "yes",
#         "no"
#     ],
#     "id": "420010",
#     "results": [
#         {
#             "text": "yes",
#             "count": 1
#         },
#         {
#             "text": "no",
#             "count": 1
#         }
#     ]
# }
