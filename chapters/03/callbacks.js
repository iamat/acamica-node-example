function doSomething (callback) {
  setTimeout(() => callback(null, 1), 100);
}

function doSomethingElse (callback) {
  setTimeout(() => callback(null, 2), 100);
}

function doSomeMore (callback) {
  setTimeout(() => callback(null, 3), 100);
}

doSomething((err, first) => {
  if (err) {
    console.error(err);
    return;
  }

  doSomethingElse((err, second) => {
    if (err) {
      console.error(err);
      return;
    }

    doSomeMore((err, third) => {
      if (err) {
        console.error(err);
        return;
      }

      console.log([first, second, third]);
    });
  });
});
