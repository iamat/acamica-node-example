var doSomething = Promise.resolve(1);
var doSomethingElse = 3;
var doSomeMore = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 3);
});

Promise.all([doSomething, doSomethingElse, doSomeMore]).then(values => {
  console.log(values);
});
