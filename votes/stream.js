const redis = require('../db/redis.js');

const util = require('util');
const { Readable } = require('stream');

function VoteScan (opt, match) {
  Readable.call(this, opt);
  this._redisCursor = '0';
  this._match = match;
  this.opt = opt;
}

util.inherits(VoteScan, Readable);

VoteScan.prototype._read = function () {
  if (this._redisDrained) {
    this.push(null);
    return;
  }

  var args = [this._redisCursor, 'MATCH', this._match];

  redis.scan(args, (err, res) => {
    if (err) return this.emit('error', err);

    // checking if cursor is null
    this._redisCursor = (res[0] instanceof Buffer) ? res[0].toString() : res[0];
    if (this._redisCursor === '0') {
      this._redisDrained = true;
    }

    // end of stream
    if (res[1].length === 0 && this._redisDrained) {
      this.push(null);
      return;
    }

    // get values for all found keys
    const commands = res[1].map((key) => ['get', key]);
    redis.multi(commands).exec((err, result) => {
      if (err) {
        this.emit('error', err);
        return;
      }
      this.push(result.map((v) => v[1]).join('\n'));
    });
  });
};

VoteScan.prototype.close = function () {
  this._redisDrained = true;
};

module.exports = VoteScan;
