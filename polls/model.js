const async = require('async');
const redis = require('../db/redis.js');

const POLL_PREFIX = 'poll';

function getAll (callback) {
  async.waterfall([(done) => {
    redis.keys(`${POLL_PREFIX}:*`, done);
  }, (keys, done) => {
    async.map(keys, (key, map) => {
      redis.get(key, (err, result) => {
        if (err) return map(err);

        let poll;
        try {
          poll = JSON.parse(result);
        } catch (e) {
          map(e);
          return;
        }

        map(null, poll);
      });
    }, done);
  }], callback);
}

function get (id, callback) {
  redis.get(`${POLL_PREFIX}:${id}`, (err, result) => {
    if (err) return callback(err);

    let poll;

    try {
      poll = JSON.parse(result);
    } catch (e) {
      return callback(e);
    }

    callback(null, poll);
  });
}

function create (poll, callback) {
  const id = Math.floor((Math.random() * 1e6)).toString();
  poll.id = id;

  redis.set(`${POLL_PREFIX}:${id}`, JSON.stringify(poll), (err, result) => {
    if (err) return callback(err);

    callback(null, { id, poll });
  });
}

function update (id, poll, callback) {
  redis.set(`${POLL_PREFIX}:${id}`, JSON.stringify(poll), (err, result) => {
    if (err) return callback(err);

    callback(null, { id, poll });
  });
}

function remove (id, callback) {
  redis.del(`${POLL_PREFIX}:${id}`, callback);
}

module.exports = {
  getAll,
  create,
  get,
  update,
  remove
};
