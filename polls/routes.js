const Polls = require('./model');

function getAll (req, res, next) {
  Polls.getAll((err, polls) => {
    if (err) return next(err);

    res.send({ polls });
  });
}

function create (req, res, next) {
  const poll = req.body;

  Polls.create(poll, (err, createdPoll) => {
    if (err) return next(err);

    req.log.info({ poll: createdPoll }, 'poll created');
    res.send({ poll: createdPoll });
  });
}

function get (req, res, next) {
  const id = req.params.id;

  Polls.get(id, (err, poll) => {
    if (err) return next(err);

    res.send({ poll });
  });
}

function update (req, res, next) {
  const id = req.params.id;
  const poll = req.body;

  Polls.update(id, poll, (err, updatedPoll) => {
    if (err) return next(err);

    req.log.info({ poll: updatedPoll }, 'poll updated');
    res.send({ updatedPoll });
  });
}

function remove (req, res, next) {
  const id = req.params.id;

  Polls.remove(id, (err) => {
    if (err) return next(err);

    req.log.info({ poll: { id } }, 'poll removed');
    res.send(204);
  });
}

module.exports = {
  getAll,
  create,
  update,
  get: get,
  remove
};
