FROM node:8

RUN mkdir /deploy
WORKDIR /deploy

# npm install first copy packag.json for cachability
ADD package.json /deploy/package.json
RUN npm install --quiet

COPY ./ /deploy/
CMD node index.js
